from setuptools import setup
from os.path import join, dirname

r_file = open('requirements.txt', 'r')
requirements = str(r_file.read()).split('\n')
r_file.close()

setup(
    name='eblitz_rpc',
    version='0.1',
    packages=['transport'],
    url='https://codefather-labs@bitbucket.org/codefather-hub/eblitz_rpc.git',
    license='GNU General Public License v3.0',
    author='codefather',
    author_email='codefather.ru@gmail.com',
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    description='',
    test_suite='tests',
    include_package_data=True,
    zip_safe=False,
    install_requires=requirements
)
