from enum import IntEnum, Enum
from typing import TypedDict, Optional, Dict, Callable, Union

from pydantic import BaseModel


class Service(Enum):
    auth = 'as'
    user = 'us'
    game = 'gs'
    team = 'ts'
    notify = 'ns'
    discord = 'ds'


class EventName(Enum):
    user_code = 'user:code'
    user_status = 'user:status'
    epicbot_invites_remain = 'epicbot:invites_remain'


class Event(BaseModel):
    name: EventName
    sender: Union[Service, None]
    listener: Union[Service, None]
    callback: Union[Callable, None]


class Key(Enum):
    """

    - первый ключ = акроним сервиса. gs = game service
    - второй ключ = модель
    - третий ключ поле
    - четвертое значение идентификатор

    Использовать:
        key = RedisKey.gs_user_status.value + f':{EPIC_ID}'
        out: 'gs:user:status:111'

    """
    gs_user_code = f'{Service.game.value}:' \
                   f'{EventName.user_code.value}'  # + f':{EPIC_ID}'

    gs_user_status = f'{Service.game.value}:' \
                     f'{EventName.user_status.value}'  # + f':{EPIC_ID}'

    gs_epicbot_invites_remain = f'{Service.game.value}:' \
                                f'{EventName.epicbot_invites_remain.value}'  # + f':{BOT_EMAIL}'

    @staticmethod
    async def format(key, identificator) -> str:
        return f"{key.value}:{identificator}"


class RPCResponseStatus(IntEnum):
    success = 1
    failure = 2


class RPCRequest(BaseModel):
    key: Key
    event: Event
    data: Union[Dict, None]
    expire: Union[int, None]

    @staticmethod
    async def transport_deserialize(data: bytes):
        data: list = str(data.decode()).split('-')
        event: Event = Event(**{
                "name": data[1],
                "sender": data[2],
                "listener": data[3],
            })
        if isinstance(data[4], Callable):
            event.callback = data[4]
        if isinstance(data[5], dict):
            d = data[5]
        else:
            d = None
        return {
            "key": data[0],
            "event": event,
            "data": d,
            "expire": data[6]
        }

    @property
    async def transport_serialize(self) -> bytes:
        return f"{self.key.value}-" \
               f"{self.event.name.value}-" \
               f"{self.event.sender.value}-" \
               f"{self.event.listener.value}-" \
               f"{self.event.callback}-" \
               f"{self.data}-" \
               f"{self.expire}".encode()


class RPCResponse(BaseModel):
    data: Optional[Dict]
    status: RPCResponseStatus


class Target(Enum):
    listener = 'listener'
    sender = 'sender'
