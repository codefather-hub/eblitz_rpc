async def generator(subjects: list):
    i = 0
    len_a = len(subjects)
    while i < len_a:
        yield subjects[i]
        i += 1
