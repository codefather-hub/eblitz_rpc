import base64
import json
import os
import asyncio
import logging
import re
from ast import literal_eval
from asyncio import Queue
from multiprocessing.dummy import Process

import aioredis

from threading import Thread
from typing import Union, Coroutine, List, Optional

import aioredis
import uvloop
from aioredis.abc import AbcChannel

from transport.models import (
    RPCRequest, RPCResponse, RPCResponseStatus, Service, Event, Key, EventName, Target
)
from transport.utils import generator

uvloop.install()


class RPCController:

    # TODO Подключиться к редису
    # TODO Подключить Producer
    # TODO Подключить Consumer
    # TODO Подключить евенты

    # https://docs.python.org/3/library/asyncio-queue.html#examples
    # https://gist.github.com/jmbjorndalen/e1cbd93c475792c83f79ef475345ed00

    def __init__(self, service_name: Service, loop: asyncio.AbstractEventLoop,
                 target: Target, subscribe_events: List[Event] = None):

        self.target: Target = target
        self.subscribe_events: List[Event] = subscribe_events
        self.service_name: Service = service_name
        self.loop: asyncio.AbstractEventLoop = loop

        self.channels: Union[List[aioredis.Channel], None] = None
        self.target_coro: Union[Coroutine, None] = None
        self.redis: Union[aioredis.commands.Redis, None] = None
        self.last_request: Union[RPCRequest, None] = None

        self.listener: aioredis.pubsub.Receiver = aioredis.pubsub.Receiver()
        self.logger = logging.Logger(name=str(self.__class__), level=logging.INFO)

        self.process: Process = Process(target=self.loop.create_task, args=([self.start()]))

    def stop(self):
        self.process.close()

    async def start(self):
        self.redis = await aioredis.create_redis_pool(os.environ.get('REDIS_HOST'))
        if self.subscribe_events:
            channels = [
                event.name.value async for event in generator(self.subscribe_events)
            ]
            self.channels = await self.redis.subscribe(*channels)

        if self.target == Target.listener:
            await self.fetch_listener()

        elif self.target == Target.sender:
            # await self.fetch_sender()
            ...

    async def fetch_listener(self):
        while True:

            async for channel in generator(self.channels):
                self.loop.create_task(self.read(channel))

                await asyncio.sleep(5)

            await asyncio.sleep(1)

    async def fetch_sender(self):
        while True:
            async for channel in generator(self.channels):
                # DEBUG TESTING
                # request: RPCRequest = RPCRequest(
                #     key=Key.gs_user_status.value,
                #     event=Event(
                #         name=EventName.user_status.value,
                #         sender=Service.game.value,
                #         listener=Service.auth.value,
                #         callback=None
                #     ),
                #     data={},
                #     expire=60
                # )
                # await self.redis.publish(
                #     channel=channel.name,
                #     message=await request.transport_serialize
                # )
                ...
            # await asyncio.sleep(1)

    async def read(self, channel):
        async for message in channel.iter():
            message: bytes
            data: dict = await RPCRequest.transport_deserialize(data=message)
            request: RPCRequest = RPCRequest(**data)

            await self.parse_request(request=request)

    async def parse_request(self, request: RPCRequest) -> None:
        if request.event in self.subscribe_events and \
                request.event.listener == self.service_name and \
                self.last_request != request:

            print(f"service: {self.service_name} got new message")
            self.last_request = request

        else:
            await self.send_message(request=request)

    async def send_message(self, request: RPCRequest) -> RPCResponse:
        if isinstance(request, RPCRequest):

            async for channel in generator(self.channels):
                channel: aioredis.pubsub.Channel

                if channel.name.decode() == request.event.name.value:
                    request.event.sender = self.service_name

                    await self.redis.publish(
                        channel=channel.name,
                        message=await request.transport_serialize
                    )

            # print(f"Success send message: {request}")

            return RPCResponse(status=RPCResponseStatus.success, data={})
        else:
            self.logger.error(f"Failed send message: {request}")
            return RPCResponse(status=RPCResponseStatus.failure, data={
                "message": "request is not valid"
            })
